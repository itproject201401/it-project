# README #

This README would normally document whatever steps are necessary to get your application up and running.

###Game application for COMP30022, 2014 Semester 2###

###Game Name: Office War###
###Group Number: 1###
###Group members: Dieu Anh, Michael Pham, Kelly Yang, Ha Jin Song###
###Target Android version- 4.2.2 and up###
###Engine used for development: AndEngine###

###Office War is game where user can either player single or multi-player mode###

* Object of the game is to throw an object at the target without being caught by the teacher entity
* For challenge purpose, wind will disrupt the course of the trajectory of thrown objects
* User can unlock more objects to throw as their highscore accumulates (NOTE, this function is not fully implemented yet, profile updating needs brushing)
* In multi-player mode, first to score 3 is the victor of the game (NOTE, this value can be changed easily later, it was set to 3 for debug purpose)

###Instruction###
* Follow GUI to navigate through the application
* For multi-player mode, please have stable connectivity without port block
* In case of error due to missing AndEngines, please pull from following GitHub address using import functionality of the eclipse
*   https://github.com/nicolasgramlich/AndEngine.git (GLES 2.0)
*   https://github.com/nicolasgramlich/AndEnginePhysicsBox2DExtension.git

###NOTES###
* Images used are from online resources
* Classroom background: jakebowkett.deviantart.com/art/Classroom-VN-Background--382304704
* Chalkboard image: www.clker.com/cliparts/o/b/X/cj/x/blackboard-hi.png
* All rights of the image belongs to authors of each image
* If this application is to be published for commercial purpose, images need to be re-acquired
* remaining images are generated or drawn from paint


###Unimplemented functionalities###
* Option
* Updating of profile not functioning properly, changing weapons are working properly.