package group01.storageHandler;

/**
 * Author: Ha Jin Song
 * Date: 10/10/2014
 * Last Modified: 12/10/2014
 * Deals with profile of the game. 
 * Write and Read from device storage
 * Stores profile as XML file
 * XML file named as OWplayerData
 * Format in XML, but initially read as String
 */

import group01.gameObjects.Player;


import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.andengine.ui.activity.BaseGameActivity;
import org.w3c.dom.Document;

import android.content.Context;


public class ProfileHandler{
	//Variables used
	//File name for application
	private String filename = "OWplayerData";
	private String doc;
	//Writing and Reading from internal storage
	FileOutputStream outputStream;
	FileInputStream inputStream;
	InputStreamReader streamReader;
	BaseGameActivity context;
	XMLHandler xmlHandler;
	Player profile;
	public ProfileHandler(BaseGameActivity activity){
		this.context = activity;
		xmlHandler = new XMLHandler();
	}
	
	/**
	 * initialise
	 * Set up profile system
	 * @throws Exception
	 */
	public void initialise() throws Exception{

		Document xmlDoc;
		//Check if a profile exist already, if not, generate a new profile
		try {
			//Profile exist, start reading into byte buffer
			//and Convert to xml format then to player
			  inputStream = context.openFileInput(filename);
			  streamReader = new InputStreamReader(inputStream);
			  byte[] inputBuffer = new byte[inputStream.available()];
			  inputStream.read(inputBuffer);
			  doc = new String(inputBuffer);
			  inputStream.close();
			  streamReader.close();
			  //String loaded from file, convert to xml format
			  xmlDoc = xmlHandler.stringToxml(doc);

			  xmlDoc.normalizeDocument();
			  //create profile from xml
			  profile = xmlHandler.processXml(xmlDoc);
			  
			  
		} catch (FileNotFoundException e1) {
			
			outputStream = context.openFileOutput(filename, Context.MODE_PRIVATE);
			ArrayList<String> newWeapons= new ArrayList<String>();
			newWeapons.add("Basic");
			//Create new profile
			Player newPlayer = new Player("John", "0", newWeapons);
			profile = newPlayer;
			//Write new profile to device storage
			doc = xmlHandler.profileToString(newPlayer);
			outputStream.write(doc.getBytes());
			outputStream.flush();
			outputStream.close();
			
			
		} catch (Exception e){
			e.printStackTrace();

		}
	
	}

	/**
	 * updateProfile
	 * updates player profile and save on to device
	 * @param player: player profile after a single player match has finished
	 */
	public void updateProfile(Player player){
		try {
			player.updateWeapon();
			context.toastOnUIThread(player.getWeapons().get(1).toString());
			String doc = xmlHandler.profileToString(player);
			outputStream = context.openFileOutput(filename, Context.MODE_PRIVATE);
			outputStream.write(doc.getBytes());
			outputStream.flush();
			outputStream.close();
		} catch (Exception e){}
		
		
	}
	public Player getProfile(){return profile;}


}
