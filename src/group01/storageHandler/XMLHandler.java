package group01.storageHandler;

/**
 * Author: Ha Jin Song
 * Date: 10/10/2014
 * Last Modified: 12/10/2014
 * Helper class for ProfileHandler.java
 * Deals with XML file manipulation
 * converts String to XML and vice versa
 * converts Player to XML and vice versa
 */

import group01.gameObjects.Player;


import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;


import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xmlpull.v1.XmlSerializer;

import android.util.Xml;

public class XMLHandler {
	
	
	private String elemProfile = "profile"; //root
	private String elemName = "name"; //name tag
	private String elemScore= "highscore"; //highscore tag
	private String elemWeaponList = "weaponList"; //weaponList tag
	private String elemWeapon ="weapon"; //weapon tag
	public XMLHandler(){
	}
	
	/**
	 * profileToString
	 * Convert profile to string of xml file
	 * @param player: profile to convert
	 * @return xml in string form
	 * @throws Exception
	 */
	public String profileToString(Player player) throws Exception{
		String tempName = player.getName();
		String tempScore = player.getScore();
		ArrayList<String> tempWeapons = player.getWeapons();
		
		//use XmlSerializer to create xml file from player data
		//Most simple to use
		XmlSerializer serial = Xml.newSerializer();
		StringWriter doc = new StringWriter();
		
		serial.setOutput(doc);
		serial.startDocument("UTF-8",Boolean.valueOf(true));
		//Indentation feature, easier to use when debugging
		serial.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);
		

		//Open root
		serial.startTag(null, elemProfile);
		
		//Open name tag, write data and close
		serial.startTag(null, elemName);
		serial.text(tempName);
		serial.endTag(null, elemName);
		
		//Open highscore tag, write data and close
		serial.startTag(null, elemScore);
		serial.text(tempScore);
		serial.endTag(null, elemScore);
		
		//Open weaponlist tag
		serial.startTag(null, elemWeaponList);
		//user can have multiple weapon
		for(String s : tempWeapons){
			serial.startTag(null,elemWeapon);
			serial.text(s);
			serial.endTag(null,elemWeapon);
			
		}
		//close weaponlist tag
		serial.endTag(null, elemWeaponList);
		//close root
		serial.endTag(null, elemProfile);
		//Finalise XML document
		serial.endDocument();

		//Return as string
		return doc.toString();
		
	}
	
	/**
	 * processXml
	 * convert XML to player profile
	 * @param doc: XML document to convert
	 * @return profile extracted from XML
	 * @throws Exception
	 */
	public Player processXml(Document doc) throws Exception{
		//Read player data from XML document
		Player player;
		//Get user Name
		String tempName = doc.getElementsByTagName(elemName).item(0).getTextContent();
		//Get user Score
		String tempScore = doc.getElementsByTagName(elemScore).item(0).getTextContent();
		//Get user weapons
		ArrayList<String> tempWeapon = new ArrayList<String>();
		NodeList tempWp = doc.getElementsByTagName(elemWeaponList);
		Node temp;
		int i;
		for(i = 0 ; i < tempWp.getLength(); i ++){
			temp = tempWp.item(i);
			tempWeapon.add(temp.getTextContent());
			
		}
		//Return player profile
		player = new Player(tempName, tempScore,tempWeapon);
		return player;
	}


	/**
	 * stringToxml
	 * convert string to xml file
	 * @param data: string to convert to xml file
	 * @return
	 */
	public Document stringToxml(String data){
		//Converts String to XML document
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();  
	    DocumentBuilder builder;  
		try{
			builder = factory.newDocumentBuilder();
			Document doc = builder.parse(new InputSource(new StringReader(data)));
			return doc;
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
}
