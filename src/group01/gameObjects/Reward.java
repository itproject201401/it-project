package group01.gameObjects;

import java.util.ArrayList;
/**
 * Author: Ha Jin Song
 * Date: 14/10/2014
 * Reward class, used to give4 players more weapons depending on their score
 */
public class Reward {
	/**
	 * updateWeaponList
	 * update weapon list given player's score
	 * @param score: player's current high score
	 * @return new weapon list, weaponList
	 */
	public ArrayList<String> updateWeaponList(int score){
		ArrayList<String> weaponList = new ArrayList<String>();
		weaponList.add("Basic");
		if(score >= 5){
			weaponList.add("Special");
		}
		
		
		return weaponList;
		
	}

}
