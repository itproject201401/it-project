package group01.gameObjects;
/**
* Author: Hoang Dieu Anh Nguyen
* Date: 5/10/2014
* Last Modified: 22/10/2014 
* Weapon for single player mode
*/
import org.andengine.engine.handler.physics.PhysicsHandler;
import org.andengine.entity.modifier.LoopEntityModifier;
import org.andengine.entity.modifier.RotationModifier;
import org.andengine.entity.modifier.ScaleModifier;
import org.andengine.entity.modifier.SequenceEntityModifier;
import org.andengine.entity.scene.IOnSceneTouchListener;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.AnimatedSprite;

import org.andengine.input.touch.TouchEvent;

import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

public class WeaponSingle extends AnimatedSprite implements IOnSceneTouchListener{
	private PhysicsHandler mPhysicsHandler;

	public float initX, initY;
	public static final int ACCE=70;

	//Change in x and y;
	private double angle;
	private double velocity;
	private float myX,myY;
	private float weaponVX;
	private float weaponVY;
	private float frameDuration;
	private boolean thrown = false;
	private boolean available = true;
	//decide where to land
	private static final double DISTANCE= 6.0/8;

	public double stopX=0.0;


	public WeaponSingle(float pX, float pY, ITiledTextureRegion pTextureRegion,
			VertexBufferObjectManager pVertexBufferObjectManager, float frameDuration) {
		super(pX, pY, pTextureRegion, pVertexBufferObjectManager);
		this.frameDuration=frameDuration;
		mPhysicsHandler = new PhysicsHandler(this);
		registerUpdateHandler(mPhysicsHandler);
		initX = pX;
		initY = pY;
		// TODO Auto-generated constructor stub
	}
	
	public float getInitX(){
		return initX;
	}
	public double getStopX(){
	    return stopX;
	}


	public PhysicsHandler getPhysics(){
    	return mPhysicsHandler;
    }
	
	public boolean getThrown(){
		return thrown;
	}



	/**
	 * onSceneTouchEvent
	 * Throw the weapon when screen is touched
	 */
	@Override
	public boolean onSceneTouchEvent(Scene pScene, TouchEvent pSceneTouchEvent) {
		//Get velocity of the weapon by finding difference between
		//Original X and Y to New X and Y
		//New X and Y being the X and Y value at point where screen gets untouched
		
		if (pSceneTouchEvent.isActionDown()&&!thrown&&available)
		{
			myX = pSceneTouchEvent.getX();
			myY = pSceneTouchEvent.getY();
			
		}else if(pSceneTouchEvent.isActionUp()&&!thrown&&available){
			weaponVX=pSceneTouchEvent.getX()-myX;
			weaponVY=pSceneTouchEvent.getY()-myY;

			velocity=Math.sqrt((weaponVX*weaponVX)+(weaponVY*weaponVY));
			angle=Math.abs(Math.toDegrees(Math.asin(weaponVY/velocity)));

			stopX= DISTANCE*(velocity*velocity*Math.sin(Math.toRadians(angle*2)))/ACCE;
			stopX=Math.abs(stopX);
			// prevent re-throwing the object
			thrown=true;
			mPhysicsHandler.setVelocity(weaponVX,weaponVY);	
			mPhysicsHandler.setAccelerationY(ACCE);
			//create3DEffect
			//this.animate(frameDuration, false);
			this.registerEntityModifier(new SequenceEntityModifier(
					(new ScaleModifier(frameDuration, 1f, 0.3f, 1f, 0.3f))));
			this.registerEntityModifier(new LoopEntityModifier(new RotationModifier(2f, 0, -360)));
		}
		return true;
	}
	
	public void setAvaiable(Boolean bool){
		available = bool;
	}
}
