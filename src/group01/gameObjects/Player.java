package group01.gameObjects;

/**
 * Author: Michael
 * Date: 10/10/2014
 * Modified by: Ha Jin Song
 * Last Modified: 12/10/2014
 * Player class used to represent player profile
 */
import java.util.ArrayList;

public class Player{
	private String name;
	private ArrayList<String> weapons;
	private int score;
	private Reward rewards;
	public Player(String name,String score, ArrayList<String> weapons){
		this.name = name;
		this.score = Integer.parseInt(score);
		this.weapons = weapons;
		this.rewards = new Reward();
	}
	/**
	 * Getters and Setters
	 */
	public String getName(){ return this.name;}
	public String getScore() { return String.valueOf(score);}
	public ArrayList<String> getWeapons() { return weapons; }
	public void setName(String name){ this.name = name;}
	public void setScore(String score) { this.score = Integer.parseInt(score);}
	
	/**
	 * updateWeapon
	 * call reward for player and update according to score
	 */
	public void updateWeapon() { 
		weapons = rewards.updateWeaponList(score);
	}
	
}
