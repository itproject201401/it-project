package group01.itproject;

/**
 * Author: Ha Jin Song
 * Date: 30/9/2014
 * Last Modified: 22/10/2014 by Hoang Dieu Anh Nguyen
 * ResourceManager, manages all resources for the game
 * Based on tutorial for AndEngine
 * http://www.matim-dev.com/tutorials.html
 */

import org.andengine.engine.Engine;
import org.andengine.engine.camera.Camera;

import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontFactory;
import org.andengine.opengl.texture.ITexture;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.atlas.bitmap.BuildableBitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.source.IBitmapTextureAtlasSource;
import org.andengine.opengl.texture.atlas.buildable.builder.BlackPawnTextureAtlasBuilder;
import org.andengine.opengl.texture.atlas.buildable.builder.ITextureAtlasBuilder.TextureAtlasBuilderException;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.texture.region.TiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;


import org.andengine.ui.activity.BaseGameActivity;
import org.andengine.util.color.Color;
import org.andengine.util.debug.Debug;


import group01.gameObjects.Player;
import group01.storageHandler.ProfileHandler;

public class ResourcesManager
{

    
	
	public static final String apiKey="0bb16e87cb2fe4980421089a81f9a08d5752dfa9f5b2fdd34c59ae094c011ded";
	public static final String secretKey="371a69515e6ef89681306f1855581131daec966bf2b92699023a3239e24d5da6";
    private static final ResourcesManager INSTANCE = new ResourcesManager();    
    public Engine engine;
    public BaseGameActivity activity;
    public Camera camera;
    public VertexBufferObjectManager vbom;
    
    
    /**
     * Atlas is form of canvas for the images
     * Region is place in that Atlas for specific image (Being called in the method)
     * Loading that Atlas will show all the images(Region) placed in that Atlas
     * Each scene can be considered as one Atlas with multiple images(Regions) in them
     */
    
    //Font
    public Font font;
    //Splash screen
    public ITextureRegion splash_region;
    private BitmapTextureAtlas splashTextureAtlas;
    
    //Start Screen
    public ITextureRegion title_bg_reg;
    public ITextureRegion target;
    public ITextureRegion strt_region;
    public ITextureRegion opt_region;
    public TiledTextureRegion input;
    private BuildableBitmapTextureAtlas titleAtlas;

    //Game Screen
    public ITextureRegion gamebg;
    public ITextureRegion pausebtn;
    public ITextureRegion selectbtn;
    private BuildableBitmapTextureAtlas gameAtlas;
    public static final float DURATION=10;
    public static final float WIND_INTERVAL=40;
    public static final String YRTURN= "Your turn, Start!";
    public static final String OTHERTURN= "Stop!Player 2's turn";
    public static final String UWIN= "YOU WIN!!";
    public static final String ULOSE= "LOSER!!";
    public static final int WINSCORE= 3;
    public static final float FLASH= 4;



    
    //Weapons
    public ITiledTextureRegion basic;
    public ITiledTextureRegion special;
    //Puase Screen, sub scene of Game Screen
    public ITextureRegion subbg;
    public ITextureRegion menubtn;
    public ITextureRegion gamebtn;

    
   //Player Profile
    public static ProfileHandler pHandler;
    public static Player player;
    

    //wind
    public  ITextureRegion teacher;
    public ITextureRegion wind;
	public ITextureRegion bluetooth_region;


    //Game Scene resource controllers
    
    public void loadGameResources()
    {
        loadGameGraphics();
        loadGameFonts();
        loadTitleFonts();
        loadGameAudio();
    }
 
    private void loadGameGraphics()
    {
    	BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("images/");
    	//Start World
    	gameAtlas = new BuildableBitmapTextureAtlas(activity.getTextureManager(), 1028, 1028, TextureOptions.BILINEAR);
    	
    	//create backgrounds
    	gamebg = BitmapTextureAtlasTextureRegionFactory.createFromAsset(gameAtlas, activity, 
    			"bg/classroombg.jpg");
    	subbg = BitmapTextureAtlasTextureRegionFactory.createFromAsset(gameAtlas, activity, "bg/igmbg.png");
    	
    	//create buttons
    	pausebtn = BitmapTextureAtlasTextureRegionFactory.createFromAsset(gameAtlas, activity, "btn/Pause.png");
    	gamebtn = BitmapTextureAtlasTextureRegionFactory.createFromAsset(gameAtlas,activity,"btn/B2G.png");
    	menubtn = BitmapTextureAtlasTextureRegionFactory.createFromAsset(gameAtlas,activity,"btn/B2M.png");
    	selectbtn = BitmapTextureAtlasTextureRegionFactory.createFromAsset(gameAtlas, activity, "btn/SelectWeapon.png");
    	
    	//create weapons
    	basic = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(gameAtlas, activity, "obj/Weapon.png", 1, 1);
    	special = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(gameAtlas, activity, "obj/Weapon2.png", 1, 1);
    	
    	
    	//create game objects
    	target = BitmapTextureAtlasTextureRegionFactory.createFromAsset(gameAtlas,activity,"obj/target.png");
    	wind = BitmapTextureAtlasTextureRegionFactory.createFromAsset(gameAtlas, activity, "obj/wind.png");
    	teacher=BitmapTextureAtlasTextureRegionFactory.createFromAsset(gameAtlas, activity, "obj/teacher.png");
    	
    	//Load profile
    	pHandler = new ProfileHandler(activity);
    	try {
			pHandler.initialise();
			player = pHandler.getProfile();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

    	try 
    	{
    	    this.gameAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 1, 0));
    	    this.gameAtlas.load();
    	} 
    	catch (final TextureAtlasBuilderException e)
    	{
    	        Debug.e(e);
    	}
    }
    public void unloadGameTextures()
    {
    	if(titleAtlas.isLoadedToHardware()){
    		titleAtlas.unload();
    	}
    }
        
    public void loadGameTextures(){
    	if(!titleAtlas.isLoadedToHardware()){
    		titleAtlas.load();
    	}
    }
    
    private void loadGameFonts()
    {
        
    }
    
    private void loadGameAudio()
    {
        
    }
 
    //////////////////////////////
    
    //Splash Screen resource Controller
    
    
    public void loadSplashScreen()
    {
    	//Prepare Atlas for Splash Screen, size of 256x256 pixel
    	BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("images/");
    	splashTextureAtlas = new BitmapTextureAtlas(activity.getTextureManager(), 256, 256, TextureOptions.BILINEAR);
    	//Load splash image
    	splash_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(splashTextureAtlas, activity, "bg/splash.png", 0, 0);
    	splashTextureAtlas.load();
    }
    
    public void unloadSplashScreen()
    {
    	splashTextureAtlas.unload();
    	splash_region = null;
    }

    //////////////////////////////////
    
    //Title screen resource controllers
    
    public void loadTitleResources()
    {
        loadTitleGraphics();
        loadTitleAudio();
        loadTitleFonts();
    }
    
    private void loadTitleFonts()
    {
    	//Fonts stored in Assets/font folder
        FontFactory.setAssetBasePath("font/");
        //Prepare 256x256 pixel Atlas for text
        final ITexture FontTexture = new BitmapTextureAtlas(activity.getTextureManager(), 256, 256, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        //Load font, its style and Color
        font = FontFactory.createStrokeFromAsset(activity.getFontManager(),
        		FontTexture, 
        		activity.getAssets(), 
        		"FacileSans.ttf", 
        		20, 
        		true, 
        		Color.WHITE.getABGRPackedInt(),
        		2.0f, 
        		Color.WHITE.getABGRPackedInt());

        font.load();
    }
    
    private void loadTitleGraphics()
    {
    	//Load Title screen, prepare Atlas of 1028x1028 pixel
    	BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("images/");
    	titleAtlas = new BuildableBitmapTextureAtlas(activity.getTextureManager(), 1028, 1028, TextureOptions.BILINEAR);
    	//Load in images used for the title screen
    	input= BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(titleAtlas, activity, "btn/Openbt.png", 1,1);
    	title_bg_reg = BitmapTextureAtlasTextureRegionFactory.createFromAsset(titleAtlas, activity, "bg/titlebg.png");
    	strt_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(titleAtlas, activity, "btn/Start.png");
    	opt_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(titleAtlas, activity, "btn/Option.png");
    	bluetooth_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(titleAtlas, activity, "btn/Openbt.png");
    	       
    	try 
    	{
    	    this.titleAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 1, 0));
    	    this.titleAtlas.load();
    	} 
    	catch (final TextureAtlasBuilderException e)
    	{
    	        Debug.e(e);
    	}
    }
    
    public void unloadTitleTextures()
    {
    	if(titleAtlas.isLoadedToHardware()){
    		titleAtlas.unload();
    	}
    }
        
    public void loadTitleTextures()
    {
    	if(!titleAtlas.isLoadedToHardware()){
    		titleAtlas.load();
    	}
    }
    
    private void loadTitleAudio()
    {
        
    }

    
    ///////////////////////
    
    /**
     * @param engine
     * @param activity
     * @param camera
     * @param vbom
     * <br><br>
     * We use this method at beginning of game loading, to prepare Resources Manager properly,
     * setting all needed parameters, so we can latter access them from different classes (eg. scenes)
     */
    public static void prepareManager(Engine engine, BaseGameActivity activity, Camera camera, VertexBufferObjectManager vbom)
    {
        getInstance().engine = engine;
        getInstance().activity = activity;
        getInstance().camera = camera;
        getInstance().vbom = vbom;
    }
    
    //---------------------------------------------
    // GETTERS AND SETTERS
    //---------------------------------------------
    
    public static ResourcesManager getInstance()
    {
        return INSTANCE;
    }
}