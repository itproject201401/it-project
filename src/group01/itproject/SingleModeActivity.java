package group01.itproject;

import group01.scenes.SingleMode;

import java.io.IOException;



import org.andengine.engine.Engine;
import org.andengine.engine.LimitedFPSEngine;
import org.andengine.engine.camera.Camera;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.WakeLockOptions;
import org.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.andengine.entity.scene.Scene;

import org.andengine.ui.activity.BaseGameActivity;

import com.shephertz.app42.gaming.multiplayer.client.WarpClient;



import android.content.Intent;
import android.view.KeyEvent;
/**
 * Author: Ha Jin Song
 * Date: 30/9/2014
 * Last Modified: 30/9/2014
 * Main activity for the app
 * http://www.matim-dev.com/tutorials.html
 */

public class SingleModeActivity extends BaseGameActivity {
	@SuppressWarnings("unused")
	//ResourceManager handles all images/fonts used in the game
	private ResourcesManager rscManager;
	final private float cameraX = 800;
	final private float cameraY = 480;
	public WarpClient theClient;
	//Camera for the game
	private Camera camera;
	private SingleMode gameScene;
	
    
	@Override
	public Engine onCreateEngine(EngineOptions pEngineOptions) 
	{
		//Set Frame rate to 60
	    return new LimitedFPSEngine(pEngineOptions, 60);
	    
	}

    @Override
    public EngineOptions onCreateEngineOptions() {
    	//Set camera and game world
    	try {
			theClient = WarpClient.getInstance();
		} catch (Exception e) {
			e.printStackTrace();
		}
        camera = new Camera(0, 0, cameraX, cameraY);

        //Landscape, resolution and place camera
        EngineOptions engineOptions = new EngineOptions(true, ScreenOrientation.LANDSCAPE_FIXED, 
        		new RatioResolutionPolicy(cameraX, cameraY), this.camera);
        engineOptions.getAudioOptions().setNeedsMusic(true).setNeedsSound(true);
        engineOptions.setWakeLockOptions(WakeLockOptions.SCREEN_ON);
        return engineOptions;

    }
     
    @Override
	public void onCreateResources(OnCreateResourcesCallback pOnCreateResourcesCallback) throws IOException{
    	//Prepare ResourceManager which handles resources, creating ResourceManager ultimately creates Resources
        ResourcesManager.prepareManager(mEngine, this, camera, getVertexBufferObjectManager());
		ResourcesManager.getInstance().loadGameResources();
        rscManager = ResourcesManager.getInstance();
        pOnCreateResourcesCallback.onCreateResourcesFinished();
    }
 
    public void onCreateScene(OnCreateSceneCallback pOnCreateSceneCallback) throws IOException{
    	//Prepare SceneManager which handles scenes. Scenes are individual Java class which are handled
    	//By SceneManager
    	gameScene = new SingleMode();
    	gameScene.setActivity(this);
		mEngine.setScene(gameScene);
		pOnCreateSceneCallback.onCreateSceneFinished(gameScene);
    }
 
    @Override
    public void onPopulateScene(Scene pScene, OnPopulateSceneCallback pOnPopulateSceneCallback) throws IOException
    {
    	//Open Splash screen until all resources are loaded into game
    	pOnPopulateSceneCallback.onPopulateSceneFinished();
    }
    
    @Override
    //Exit
    public boolean onKeyDown(int keyCode, KeyEvent event) 
    {  
        if (keyCode == KeyEvent.KEYCODE_BACK)
        {
            SceneManager.getInstance().getCurrentScene().onBackKeyPressed();
        }
        return false; 
    }
    //Exit
    protected void onDestroy() {
    	super.onDestroy();
        System.exit(0);	

    }   
    public void stopGame(){
    	theClient.disconnect();
    	Intent intent = new Intent(this, MainActivity.class);
		startActivity(intent);
    }
}
