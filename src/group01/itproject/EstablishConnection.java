//import java.io.IOException;
//import java.util.UUID;
//
//import android.app.Activity;
//import android.bluetooth.BluetoothAdapter;
//import android.bluetooth.BluetoothDevice;
//import android.bluetooth.BluetoothServerSocket;
//import android.bluetooth.BluetoothSocket;
//import android.os.Bundle;
//import android.view.Menu;
//import android.view.MenuItem;
//import android.content.BroadcastReceiver;
//import android.content.Context;
//import android.content.Intent;
//import android.content.IntentFilter;
//import android.widget.Toast;
//
//
//public class EstablishConnection extends Activity {
//
//	private BluetoothAdapter mBA = BluetoothAdapter.getDefaultAdapter();
//    private UUID uuid = UUID.fromString("57eac3bb-7d78-41a2-8326-16ae4b36ed2d");
//    private ClientConnection cConnect;
//    private ServerConnection sConnect=new ServerConnection(mBA,uuid);
//    //ConnectionEstablished flag, runs both client and server
//    //Until either one of them establish connection with another device
//    private boolean ConnectionEstablished = false;
//    
//	public void onCreate(Bundle savedInstanceSate){
//		super.onCreate(savedInstanceSate);
//		setContentView(R.layout.activity_establish_connection);
//		//No bluetooth available, shut down
//		if(!checkBlueTooth()){
//			Toast.makeText(this, "No Bluetooth available", Toast.LENGTH_SHORT).show();
//		}
//		//Restart bluetooth discoering
//	    if (mBA.isDiscovering()){
//	    	mBA.cancelDiscovery();
//	    }
//	    allowDiscovery();
//	    mBA.startDiscovery();
//	    //Run server side connection
//		sConnect.run();
//
//		//Add response to detected devices, contains client component
//	    IntentFilter intentfilter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
//	    this.registerReceiver(mReceiver, intentfilter);
//
//	}
//	@Override
//
//	public boolean onCreateOptionsMenu(Menu menu) {
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.establish_connection, menu);
//		return true;
//	}
//
//	@Override
//	public boolean onOptionsItemSelected(MenuItem item) {
//		// Handle action bar item clicks here. The action bar will
//		// automatically handle clicks on the Home/Up button, so long
//		// as you specify a parent activity in AndroidManifest.xml.
//		int id = item.getItemId();
//		if (id == R.id.action_settings) {
//			return true;
//		}
//		return super.onOptionsItemSelected(item);
//	}
//	
//	//Ask for discovery to be on
//	private void allowDiscovery(){
//		Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
//		startActivityForResult(discoverableIntent,1);
//		
//	}
//	//Check if bluetooth if available
//	private boolean checkBlueTooth(){
//		if(mBA==null){
//			return false;
//		}
//		if (!mBA.isEnabled()) {
//			Intent turnOn = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
//			startActivityForResult(turnOn, 1);
//		}
//		
//		return true;
//		
//		
//	}
//
//	private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            String action = intent.getAction(); //may need to chain this to a recognizing function
//            //Bluetooth device found and no connection has been established
//            if (BluetoothDevice.ACTION_FOUND.equals(action) && ConnectionEstablished == false){
//                // Get the BluetoothDevice object from the Intent
//            	//Get the device detected
//                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
//            
//                try{
//                	//See if it has uuid of the application
//                	//If so, connect as client
//                	if(device.getUuids().equals(uuid)){
//                		Toast.makeText(context, 
//                        		"FOUND IT "+ "\n" + device.getAddress(), 
//                        		Toast.LENGTH_SHORT).show();
//                		cConnect = new ClientConnection(device,uuid);
//                		cConnect.run();             
//                	}
//                //If not, continue on and ignore the device.
//                }catch(NullPointerException e){    
//                	Toast.makeText(context, 
//                		device.getName() + "\n" + device.getAddress(), 
//                		Toast.LENGTH_SHORT).show();
//                }
//              
//            }
//        }
//	};
//	
//	private class ClientConnection extends Thread{
//
//		private final BluetoothSocket mmSocket;
//
//		public ClientConnection(BluetoothDevice device, UUID uuid){
//			BluetoothSocket tmp = null;
//			try{
//				tmp = device.createRfcommSocketToServiceRecord(uuid);
//				
//			}catch (IOException e){}
//			mmSocket =tmp;
//			
//		}
//		public void run(){
//			System.out.println("CLIENT");
//			try{
//				mmSocket.connect();
//				ConnectionEstablished = true;
//			}
//			catch(IOException connectionEception){
//				try{
//					mmSocket.close();
//				}catch(IOException closeException){
//					return;
//				}
//			}
//		}
//		public void cancel(){
//			try{
//				mmSocket.close();
//				
//			}catch(IOException e){}
//		}
//		
//	};
//	
//	private class ServerConnection extends Thread{
//		private final BluetoothServerSocket mmServerSocket;
//		
//		public ServerConnection(BluetoothAdapter adapter, UUID uuid){
//			BluetoothServerSocket tmp = null;
//			try{
//				tmp = adapter.listenUsingRfcommWithServiceRecord("Office War", uuid);
//			}catch(IOException e){}
//			mmServerSocket = tmp;
//		}
//		public void run(){
//			//Start listening using uuid of the application
//			BluetoothSocket socket = null;
//			System.out.println("SERVER");
//			//Run while Connection is not established
//			while(!ConnectionEstablished){
//				try{
//					//Try to connect, if succeed, change the flag
//					socket = mmServerSocket.accept();
//					ConnectionEstablished = true;
//				}catch(IOException e){
//					break;
//				}
//				if(socket != null){
//					try {
//						mmServerSocket.close();
//					} catch (IOException e) {}
//					break;
//				}
//			}
//		}
//		public void cancel(){
//			try{
//				mmServerSocket.close();
//			}catch (IOException e){}
//		}
//	};
//}
package group01.itproject;

import android.os.Bundle;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import java.util.Set;
import android.content.Intent;
import android.content.IntentFilter;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class EstablishConnection extends Activity {

   private static final int REQUEST_ENABLE_BT = 1;
   private Button onBtn;
   private TextView text;
   private BluetoothAdapter myBluetoothAdapter;
   private Set<BluetoothDevice> pairedDevices;
   private ListView myListView;
   private ArrayAdapter<String> BTArrayAdapter;
  
   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_game);
      
      // take an instance of BluetoothAdapter - Bluetooth radio
      myBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
      if(myBluetoothAdapter == null) {
    	  onBtn.setEnabled(false);
    	  text.setText("Status: not supported");
    	  
    	  Toast.makeText(getApplicationContext(),"Your device does not support Bluetooth",
         		 Toast.LENGTH_LONG).show();
    	  } else {
	      onBtn = (Button)findViewById(R.id.turnOn);
	      onBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				on(v);
			}
	      });
	      
	
	      // create the arrayAdapter that contains the BTDevices, and set it to the ListView
	      BTArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);
	      myListView.setAdapter(BTArrayAdapter);
      }
   }

   public void on(View view){
      if (!myBluetoothAdapter.isEnabled()) {
         Intent turnOnIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
         startActivityForResult(turnOnIntent, REQUEST_ENABLE_BT);

         Toast.makeText(getApplicationContext(),"Bluetooth turned on" ,
        		 Toast.LENGTH_LONG).show();
      }
      else{
         Toast.makeText(getApplicationContext(),"Bluetooth is already on",
        		 Toast.LENGTH_LONG).show();
      }
   }
   
   @Override
   protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	   // TODO Auto-generated method stub
	   if(requestCode == REQUEST_ENABLE_BT){
		   if(myBluetoothAdapter.isEnabled()) {
			   text.setText("Status: Enabled");
		   } else {   
			   text.setText("Status: Disabled");
		   }
	   }
   }
   
   public void list(View view){
	  // get paired devices
      pairedDevices = myBluetoothAdapter.getBondedDevices();
      
      // put it's one to the adapter
      for(BluetoothDevice device : pairedDevices)
    	  BTArrayAdapter.add(device.getName()+ "\n" + device.getAddress());

      Toast.makeText(getApplicationContext(),"Show Paired Devices",
    		  Toast.LENGTH_SHORT).show();
      
   }
   
   final BroadcastReceiver bReceiver = new BroadcastReceiver() {
	    public void onReceive(Context context, Intent intent) {
	        String action = intent.getAction();
	        // When discovery finds a device
	        if (BluetoothDevice.ACTION_FOUND.equals(action)) {
	             // Get the BluetoothDevice object from the Intent
	        	 BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
	        	 // add the name and the MAC address of the object to the arrayAdapter
	             BTArrayAdapter.add(device.getName() + "\n" + device.getAddress());
	             BTArrayAdapter.notifyDataSetChanged();
	        }
	    }
	};
	
   public void find(View view) {
	   if (myBluetoothAdapter.isDiscovering()) {
		   // the button is pressed when it discovers, so cancel the discovery
		   myBluetoothAdapter.cancelDiscovery();
	   }
	   else {
			BTArrayAdapter.clear();
			myBluetoothAdapter.startDiscovery();
			
			registerReceiver(bReceiver, new IntentFilter(BluetoothDevice.ACTION_FOUND));	
		}    
   }
   
		
}



