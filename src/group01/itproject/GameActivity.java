package group01.itproject;

import group01.gameObjects.Weapon;
import group01.scenes.GameScene;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.andengine.engine.Engine;
import org.andengine.engine.LimitedFPSEngine;
import org.andengine.engine.camera.Camera;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.WakeLockOptions;
import org.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.andengine.entity.scene.IOnSceneTouchListener;
import org.andengine.entity.scene.Scene;
import org.andengine.input.touch.TouchEvent;

import org.andengine.ui.activity.BaseGameActivity;
import org.json.JSONObject;

import com.shephertz.app42.gaming.multiplayer.client.WarpClient;
import com.shephertz.app42.gaming.multiplayer.client.command.WarpResponseResultCode;
import com.shephertz.app42.gaming.multiplayer.client.events.ChatEvent;
import com.shephertz.app42.gaming.multiplayer.client.events.LiveRoomInfoEvent;
import com.shephertz.app42.gaming.multiplayer.client.events.LobbyData;
import com.shephertz.app42.gaming.multiplayer.client.events.MoveEvent;
import com.shephertz.app42.gaming.multiplayer.client.events.RoomData;
import com.shephertz.app42.gaming.multiplayer.client.events.RoomEvent;
import com.shephertz.app42.gaming.multiplayer.client.events.UpdateEvent;
import com.shephertz.app42.gaming.multiplayer.client.listener.NotifyListener;
import com.shephertz.app42.gaming.multiplayer.client.listener.RoomRequestListener;

import android.content.Intent;
import android.util.Log;
import android.view.KeyEvent;

/**
 * Author:Hoang Dieu Anh Nguyen
 * created: 6/10/2014
 * Last Modified: 22/10/2014
 * Activity that controls gamescene-Multiplayer mode
 * Reference: http://appwarp.shephertz.com/realtime-multiplayer-andengine/
 */

public class GameActivity extends BaseGameActivity implements
		IOnSceneTouchListener, RoomRequestListener, NotifyListener {
	@SuppressWarnings("unused")
	// ResourceManager handles all images/fonts used in the game
	public static final int ADDSCORE = 1;
	public static final String START = "Start";

	private ResourcesManager rscManager;
	final private float cameraX = 800;
	final private float cameraY = 480;
	// Camera for the game
	private Camera camera;
	private String roomId = "";
	public WarpClient theClient;
	private GameScene gameScene;
	private HashMap<String, Integer> userMap = new HashMap<String, Integer>();
	public HashMap<String, Integer> userScores = new HashMap<String, Integer>();
	public HashMap<String, Integer> turn = new HashMap<String, Integer>();
	private float myX = 0, myY = 0;
	private float weaponVX = 0;
	private float weaponVY = 0;
	private String owner;
	

	public GameActivity() {
		userMap = new HashMap<String, Integer>();
		userScores = new HashMap<String, Integer>();
		turn = new HashMap<String, Integer>();
	}
	@Override
	public Engine onCreateEngine(EngineOptions pEngineOptions) {
		// Set Frame rate to 60
		return new LimitedFPSEngine(pEngineOptions, 60);

	}

	@Override
	public EngineOptions onCreateEngineOptions() {
		// Set camera and game world
		try {
			theClient = WarpClient.getInstance();
		} catch (Exception e) {
			e.printStackTrace();
		}

		camera = new Camera(0, 0, cameraX, cameraY);

		// Landscape, resolution and place camera
		EngineOptions engineOptions = new EngineOptions(true,
				ScreenOrientation.LANDSCAPE_FIXED, new RatioResolutionPolicy(
						cameraX, cameraY), this.camera);
		engineOptions.getAudioOptions().setNeedsMusic(true).setNeedsSound(true);
		engineOptions.setWakeLockOptions(WakeLockOptions.SCREEN_ON);
		return engineOptions;

	}

	@Override
	public void onCreateResources(
			OnCreateResourcesCallback pOnCreateResourcesCallback)
			throws IOException {
		// Prepare ResourceManager which handles resources, creating
		// ResourceManager ultimately creates Resources
		ResourcesManager.prepareManager(mEngine, this, camera,
				getVertexBufferObjectManager());
		ResourcesManager.getInstance().loadGameResources();
		rscManager = ResourcesManager.getInstance();

		pOnCreateResourcesCallback.onCreateResourcesFinished();
	}
	// join room and listen to all room's requests
	private void init(String roomId) {
		if (theClient != null) {
			theClient.addRoomRequestListener(this);
			theClient.addNotificationListener(this);
			theClient.joinRoom(roomId);
		}
	}

	public void onCreateScene(OnCreateSceneCallback pOnCreateSceneCallback)
			throws IOException {
		// Prepare SceneManager which handles scenes. Scenes are individual Java
		// class which are handled
		// By SceneManager

		SceneManager.getInstance().createSplashScene(pOnCreateSceneCallback);
	}

	@Override
	public void onPopulateScene(Scene pScene,
			OnPopulateSceneCallback pOnPopulateSceneCallback)
			throws IOException {
		// Open Splash screen until all resources are loaded into game
		mEngine.registerUpdateHandler(new TimerHandler(2f,
				new ITimerCallback() {
					public void onTimePassed(final TimerHandler pTimerHandler) {
						mEngine.unregisterUpdateHandler(pTimerHandler);

						gameScene = new GameScene();
						mEngine.setScene(gameScene);
						Intent intent = getIntent();
						// join room
						roomId = intent.getStringExtra("roomId");
						init(roomId);
						
					}
				}));
		pOnPopulateSceneCallback.onPopulateSceneFinished();

	}
	// display weapon on the screen if another user join in
	public void addMorePlayer(boolean isMine, String userName) {
		if (userMap != null) {
			if (userMap.get(userName)!=null) {// if already in room
				return;
			}
			else{
				
			    if (isMine) {
			    	gameScene.initActivity(this);
			    	gameScene.createWeapon(gameScene.weaponTexture, Utils.userIndex);
				    gameScene.setOnSceneTouchListener(this);
				    userMap.put(userName, Utils.userIndex);
				    if (Utils.userIndex==0){ 	
				    	gameScene.createSpriteSpawnTimeHandler();
				    }
				    if (userMap.size()==2){
				    	// start the game as soon as user 2 joins
				    	theClient.startGame();
				    	sendChange(START);
				    }
				    
			    }
			    else{
			    	int num=0;
			    	if (Utils.userIndex==0){
			    		num=1;
			    	}
			    	// add user to a hashmap
			    	userMap.put(userName, num);
			    	gameScene.createWeapon(gameScene.weaponTexture, num);
			    }
			    // all user score =0
			    userScores.put(userName,0);
			    
			}
			
		}
		
	}

	/**
	 * update event to all player in rooms
	 * call this function only when we want to up date position and 
	 * velocity of weapons
	 */
	private void sendUpdateEvent(float weaponVX, float weaponVY) {
		try {
			JSONObject object = new JSONObject();
			object.put("X", weaponVX + "");
			object.put("Y", weaponVY + "");
		    theClient.sendChat(object.toString());
			
			
		} catch (Exception e) {
			Log.d("sendUpdateEvent", e.getMessage());
		}
	}
	
	// send message to other user
   public void sendChange(String msg){
	   JSONObject object = new JSONObject();
	   try {
		  
		   object.put("msg", msg);
		   theClient.sendChat(object.toString());
			
			
		} catch (Exception e) {
			Log.d("sendUpdateEvent", e.getMessage());
		
	  
		}
	   
   }
	@Override
	// Exit
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			SceneManager.getInstance().getCurrentScene().onBackKeyPressed();
		}
		return false;
	}

	// Exit
	protected void onDestroy() {
		super.onDestroy();
		System.exit(0);

	}
	
    /**
     * Function handle when a message is sent to all user in the room
     */
	@Override
	public void onChatReceived(ChatEvent event) {
		// TODO Auto-generated method stub
		String sender = event.getSender();
		int currentIndex = userMap.get(sender);
		String message = event.getMessage();
		try {
			JSONObject object = new JSONObject(message);
			// update X and Y velocity of the weapon
			if (object.length()>1){
				if(!sender.equals(Utils.userName)){
					float weaponVX = Float.parseFloat(object.get("X") + "");
					float weaponVY = Float.parseFloat(object.get("Y") + "");
					Weapon opponentWeapon=gameScene.weapon.get(currentIndex); 
					opponentWeapon.flyAway(weaponVX, weaponVY);
				}
			}
			else{
				// cereate wind
			    String msg=object.getString("msg");
			    if (GameScene.WIND.equals(msg)){
			    	gameScene.createWind();
			    }
			    // user has left room
			    if(msg.equals("1")||msg.equals("0")){
			    	int i=Integer.parseInt(msg);
			    	gameScene.weapon.get(i).detachSelf();
			    	gameScene.weapon.get(i).dispose();
			    	gameScene.weapon.remove(i);
			    	userMap.remove(sender);
			    	userScores.remove(sender);
			    	userScores.put(Utils.userName,0);
			    	// if the owner of the room has left then destroy the room
			    	if (i==0 && Utils.userIndex==1){
			    		theClient.leaveRoom(roomId);
			    		theClient.deleteRoom(roomId);
						theClient.disconnect();
						// delete room as well
						Intent intent = new Intent(this, MainActivity.class);
						
						startActivity(intent);
			    	}
			    }
			    // update Scores
			    if(msg.equals(GameScene.HIT)){
			    	userScores.put(sender, userScores.get(sender)+ADDSCORE);
			    	if(sender.equals(Utils.userName)){
			    		
			    		gameScene.addToScore(userScores.get(sender));
			    	}
			    	else{
			    		gameScene.addToOtherScore(userScores.get(sender));
			    	}
			    	
			    }
			    // Start a game
			    if(msg.equals(START)){
			    	for (Map.Entry<String, Integer> entry : userScores.entrySet()) {
			    	    userScores.put(entry.getKey(), 0);
			    	}
			    	if (Utils.userName.equals(sender)){
                        gameScene.addToOtherScore(0);
                        gameScene.addToScore(0);
                        Utils.isTurn=false;
						gameScene.anounceText(ResourcesManager.OTHERTURN);
					}
					else{
                        gameScene.addToOtherScore(0);
                        gameScene.addToScore(0);
                        Utils.isTurn=true;
						gameScene.anounceText(ResourcesManager.YRTURN);
					}
			    }
			    //for teacher
			    
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		

	}
/**
 * update room property
 */
	public void getHit(String msg)
	{
    	sendChange(msg);
    	
	}
	
	@Override
	public void onGameStarted(String arg0, String arg1, String arg2) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onGameStopped(String arg0, String arg1) {
		// TODO Auto-generated method stub

	}
    // after 30 seconds , client calls this function to pass turn to other player
	// this function check if a player wins or not
	@Override
	public void onMoveCompleted(MoveEvent event) {
		String nextTurn= event.getNextTurn();
		String sender= event.getSender();
		if (!Utils.userName.equals(nextTurn)){
			if (userScores.get(sender)>=ResourcesManager.WINSCORE){
				gameScene.anounceText(ResourcesManager.UWIN);
				
			}
			else{
			Utils.isTurn=false;
			gameScene.anounceText(ResourcesManager.OTHERTURN);
			}
		}
		else{
			if (userScores.get(sender)>=ResourcesManager.WINSCORE){
				gameScene.anounceText(ResourcesManager.ULOSE);
			}
			else{
				Utils.isTurn=true;
				gameScene.anounceText(ResourcesManager.YRTURN);
			}
		}
	}

	@Override
	public void onPrivateChatReceived(String arg0, String arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPrivateUpdateReceived(String arg0, byte[] arg1, boolean arg2) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onRoomCreated(RoomData arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onRoomDestroyed(RoomData arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onUpdatePeersReceived(UpdateEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	//we can check if a player wins or not here
	public void onUserChangeRoomProperty(RoomData roomData, String userName,
			HashMap<String, Object> tableProperties, HashMap<String, String> lockProperties) {
		// TODO Auto-generated method stub
		
		
	}

	@Override
	public void onUserJoinedLobby(LobbyData arg0, String arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onUserJoinedRoom(RoomData arg0, String name) {
		// TODO Auto-generated method stub
		if(name.equals(Utils.userName)){
			addMorePlayer(true, name);
		}
		else{
			addMorePlayer(false, name);
		}
		
	}

	@Override
	public void onUserLeftLobby(LobbyData arg0, String arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onUserLeftRoom(RoomData arg0, String arg1) {
		// TODO Auto-generated method stub

			
	}

	@Override
	public void onUserPaused(String arg0, boolean arg1, String arg2) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onUserResumed(String arg0, boolean arg1, String arg2) {
		// TODO Auto-generated method stub

	}
    
	// update room property
	@Override
	public void onGetLiveRoomInfoDone(LiveRoomInfoEvent event) {
		// TODO Auto-generated method stub
		if (event.getResult() == WarpResponseResultCode.SUCCESS) {
			String[] joinedUser = event.getJoinedUsers();
			if (joinedUser != null) {
				if(joinedUser.length==0){
					theClient.deleteRoom(roomId);
				}
				for (int i = 0; i < joinedUser.length; i++) {
					if (joinedUser[i].equals(Utils.userName)) {
						addMorePlayer(true, joinedUser[i]);
					} else {
						addMorePlayer(false, joinedUser[i]);
					}
				}
			}
			// update score
		} else {
			Utils.showToastOnUIThread(this, "onGetLiveRoomInfoDone: try again, weak connection "
					+ event.getResult());
		}

	}

	@Override
	public void onJoinRoomDone(RoomEvent event) {
		// TODO Auto-generated method stub
		if (event.getResult() == WarpResponseResultCode.SUCCESS) {
			theClient.subscribeRoom(roomId);
		} else {
			Utils.showToastOnUIThread(this,
					"onJoinRoomDone: weak connection " + event.getResult());
		}
	}

	@Override
	public void onLeaveRoomDone(RoomEvent event) {
		


	}

	@Override
	public void onLockPropertiesDone(byte arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onSetCustomRoomDataDone(LiveRoomInfoEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onSubscribeRoomDone(RoomEvent event) {
		// TODO Auto-generated method stub
		if (event.getResult() == WarpResponseResultCode.SUCCESS) {
			theClient.getLiveRoomInfo(roomId);
			owner=event.getData().getRoomOwner();
		} else {
			Utils.showToastOnUIThread(this, "onSubscribeRoomDone: weak connection "
					+ event.getResult());
		}

	}

	@Override
	public void onUnSubscribeRoomDone(RoomEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onUnlockPropertiesDone(byte arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onUpdatePropertyDone(LiveRoomInfoEvent arg0) {
		// TODO Auto-generated method stub

	}
    // listen to touch event on screen
	@Override
	public boolean onSceneTouchEvent(Scene pScene, TouchEvent pSceneTouchEvent) {
		// Get velocity of the weapon by finding difference between
		// Original X and Y to New X and Y
		// New X and Y being the X and Y value at point where screen gets
		// untouched
		Weapon myWeapon = gameScene.weapon.get(Utils.userIndex);
		if(Utils.isTurn==true){
			if (pSceneTouchEvent.isActionDown() && !myWeapon.getThrown()
				&& myWeapon.available) {
				myX = pSceneTouchEvent.getX();
				myY = pSceneTouchEvent.getY();

			} else if (pSceneTouchEvent.isActionUp() && !myWeapon.getThrown()
				&& myWeapon.available) {
				weaponVX = pSceneTouchEvent.getX() - myX;
				weaponVY = pSceneTouchEvent.getY() - myY;
				sendUpdateEvent(weaponVX, weaponVY);
				myWeapon.flyAway(weaponVX, weaponVY);
			
			}
		}
		
		return true;
	}
	// stop a game, return to main screen
	public void stopGame(){
		try{
			JSONObject object = new JSONObject();
			object.put("msg", Utils.userIndex +"");
			theClient.sendChat(object.toString());
			theClient.leaveRoom(roomId);
			theClient.disconnect();
			Intent intent = new Intent(this, MainActivity.class);
			startActivity(intent);
		}
		catch(Exception e){
			Log.d("delete game", e.getMessage());
		
		}
	}

}