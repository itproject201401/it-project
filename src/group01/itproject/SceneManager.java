package group01.itproject;
import group01.itproject.ResourcesManager;
import group01.scenes.BaseScene;
import group01.scenes.GameScene;
import group01.scenes.LoadingScreen;
import group01.scenes.SplashScene;
import group01.scenes.TitleScene;

import org.andengine.engine.Engine;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.ui.IGameInterface.OnCreateSceneCallback;
/**
 * Author: Ha Jin Song
 * Date: 30/9/2014
 * Last Modified: 30/9/2014
 * SceneManager, manages all scene for the game
 * Based on tutorial for AndEngine
 * http://www.matim-dev.com/tutorials.html
 */


public class SceneManager
{

    
    private BaseScene splashScene;
    private BaseScene titleScene;
    private BaseScene gameScene;
    private BaseScene loadingScene;


    private static final SceneManager INSTANCE = new SceneManager();
    
    //Game starts with Splash Screen, so initiate currentScene with
    //SCENE_SPLASH
    private SceneType currentSceneType = SceneType.SCENE_SPLASH;
    
    private BaseScene currentScene;
    
    private Engine engine = ResourcesManager.getInstance().engine;
	
	
    
    //Types of scene that are to be created in the game
    public enum SceneType
    {
        SCENE_SPLASH,
        SCENE_TITLE,
        SCENE_GAME,
        SCENE_LOADING,

    }
    

    public void setScene(BaseScene scene)
    {
        engine.setScene(scene);
        currentScene = scene;
        currentSceneType = scene.getSceneType();
    }
    
    public void setScene(SceneType sceneType)
    {
        switch (sceneType)
        {
            case SCENE_TITLE:
                setScene(titleScene);
                break;
            case SCENE_GAME:
                setScene(gameScene);
                break;
            case SCENE_SPLASH:
                setScene(splashScene);
                break;
            case SCENE_LOADING:
                setScene(loadingScene);
                break;
            default:
                break;
        }
    }
    
    //---------------------------------------------
    // GETTERS AND SETTERS
    //---------------------------------------------
    
    public static SceneManager getInstance()
    {
        return INSTANCE;
    }
    
    public SceneType getCurrentSceneType()
    {
        return currentSceneType;
    }
    
    public BaseScene getCurrentScene()
    {
        return currentScene;
    }
    

    //SPLASH SCREEN////////////////////
    /**
     * loads splash Screen
     * Splash screen already created, all it is doing is
     * Load resource of splash screen from ResourceManager
     * Get splash screen from SplashScene java class
     * Then set current scene as that
     * @param pOnCreateSceneCallback
     */
    public void createSplashScene(OnCreateSceneCallback pOnCreateSceneCallback)
    {
        ResourcesManager.getInstance().loadSplashScreen();
        splashScene = new SplashScene();
        currentScene = splashScene;
        pOnCreateSceneCallback.onCreateSceneFinished(splashScene);
    }
    /**
     * Splash Screen no longer needed, free them from resource
     * And remove the Scene
     */
    private void disposeSplashScene()
    {
        ResourcesManager.getInstance().unloadSplashScreen();
        splashScene.disposeScene();
        splashScene = null;
    }
   
    /////////////////////////////////
    
    //Title Screen///////////////////
    /**
     * load Title Screen
     * Similar to Splash screen, get resources from ResourceManager
     * And title screen from TitleScene java class
     * If it takes long to load resources into, view loading scene before title
     * Remove Splash Scene
     */
    //public void createTitleScene()
    //{
        //ResourcesManager.getInstance().loadTitleResources();
        //titleScene = new TitleScene();
        //loadingScene = new LoadingScreen();
        //SceneManager.getInstance().setScene(titleScene);
        //disposeSplashScene();
    //}
    public void createTitleScene()
    {
        ResourcesManager.getInstance().loadTitleResources();
        titleScene = new TitleScene();
        loadingScene = new LoadingScreen();
        SceneManager.getInstance().setScene(titleScene);
        //pOnCreateSceneCallback.onCreateSceneFinished(titleScene);
        //return titleScene;
        
        //disposeSplashScene();
    }
    
    /**
     * loadTitleScene
     * @param mEngine
     * show loadingScene until all resources are loaded for titleScene
     */
    public void loadTitleScene(final Engine mEngine){
    	
       	//Load loading screen while resources are being loaded for game scene
        setScene(loadingScene);
        //Get rid of game screen
        ResourcesManager.getInstance().unloadGameTextures();
        mEngine.registerUpdateHandler(new TimerHandler(0.1f, new ITimerCallback() 
        {
            public void onTimePassed(final TimerHandler pTimerHandler) 
            {
                mEngine.unregisterUpdateHandler(pTimerHandler);
                ResourcesManager.getInstance().loadTitleResources();
                titleScene = new TitleScene();
                setScene(titleScene);
            }
        }));
    }
    
    /////////////////////////////////

    //Game Screen////////////////////
    /**
     * loadGameScene
     * @param mEngine
     * Show loadingScreen until all resources are loaded for GameScene
     */
    public void loadGameScene(final Engine mEngine)
    {
    	//Load loading screen while resources are being loaded for game scene
        setScene(loadingScene);
        //Get rid of title screen
        //ResourcesManager.getInstance().unloadTitleTextures();
        mEngine.registerUpdateHandler(new TimerHandler(0.1f, new ITimerCallback() 
        {
            public void onTimePassed(final TimerHandler pTimerHandler) 
            {
                mEngine.unregisterUpdateHandler(pTimerHandler);
                ResourcesManager.getInstance().loadGameResources();
                gameScene = new GameScene();
                setScene(gameScene);
            }
        }));
    }

    
    /////////////////////////////////


}