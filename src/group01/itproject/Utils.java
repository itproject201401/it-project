package group01.itproject;
/**
* Author:Hoang Dieu Anh Nguyen
* created: 6/10/2014
* Last Modified: 22/10/2014
* Alert messages
* Reference: http://appwarp.shephertz.com/realtime-multiplayer-andengine/
*/
import java.util.List;

import org.andengine.engine.camera.Camera;

import android.app.Activity;
import android.app.ProgressDialog;

import android.widget.Toast;


public class Utils {
	// User name ( user input)
	public static String userName = "";
	// user index =1 if he is a second player, =0 if he is the owner of the room
	public static int userIndex = 1;
	public static int otherIndex=0;
	// control player's turn
	public static boolean isTurn=true;
	
	public static float getPercentFromValue(float number, float amount){
		float percent = (number/amount)*100;
		return percent;
	}
	
	public static float getValueFromPercent(float percent, float amount){
		float value = (percent/100)*amount;
		return value;
	}
	
	public static void showToastAlert(Activity ctx, String alertMessage){
		Toast.makeText(ctx, alertMessage, Toast.LENGTH_SHORT).show();
	}
	
	public static void showToastOnUIThread(final Activity ctx, final String alertMessage){
		ctx.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				Toast.makeText(ctx, alertMessage, Toast.LENGTH_SHORT).show();
				
			}
		});
	}
	
}
