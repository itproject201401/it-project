package group01.itproject;


import group01.scenes.SceneBG;

import java.util.ArrayList;

import org.andengine.entity.scene.menu.MenuScene;
import org.andengine.entity.scene.menu.item.IMenuItem;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;


/**
 * 
 * @author Ha Jin Song
 * Data created: 7/10/14
 * Last Modified: 8/10/14
 * Creates buttons/clickable objects that goes on the scene
 */
public abstract class UICreator {
	protected float sceneX,sceneY;
	protected MenuScene curScene;
	protected SceneBG bg;
	protected ArrayList<IMenuItem> buttons;
	
	public UICreator(MenuScene curScene,SceneBG bg){
		this.sceneX = bg.getX();
		this.sceneY = bg.getY();	
		this.curScene = curScene;
		this.bg = bg;
		buttons = new ArrayList<IMenuItem>();
	}
	
	public abstract void addButtons(int val, ITextureRegion texture, VertexBufferObjectManager vbom);
	
	public ArrayList<IMenuItem> getBtnList(){
		return buttons;
	}
	
}
