package group01.itproject;

/**
 * Author: Ha Jin Song
 * Date: 7/10/2014
 * Last Modified: 10/10/2014
 * Helper class used to create UI for pause menu
 * Extends UICreator
 */

import group01.scenes.SceneBG;

import org.andengine.entity.scene.menu.MenuScene;
import org.andengine.entity.scene.menu.item.IMenuItem;
import org.andengine.entity.scene.menu.item.SpriteMenuItem;
import org.andengine.entity.scene.menu.item.decorator.ScaleMenuItemDecorator;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

public class MenuUICreator extends UICreator{
	private float curX;
	private float curY;
	public MenuUICreator(MenuScene curScene, SceneBG bg, float curX, float curY) {
		super(curScene, bg);
		this.curX = curX;
		this.curY = curY;
	}

	

	@Override
	public void addButtons(int val, ITextureRegion texture,
			VertexBufferObjectManager vbom) {
		
		IMenuItem temp= new ScaleMenuItemDecorator(
	    		new SpriteMenuItem(val, texture, vbom), 1.2f, 1);
		curScene.addMenuItem(temp);
    	temp.setPosition(curX, curY);
    	curY += temp.getHeight()*1.5;
    	buttons.add(temp);
		
	}


}
