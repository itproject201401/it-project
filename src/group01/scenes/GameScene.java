package group01.scenes;

/**
 * Author:Hoang Dieu Anh Nguyen
 * Date: 2/10/2014
 * Last Modified: 22/10/2014
 * GameScene, main Scene for multiplayer Mode
 */
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import group01.itproject.GameActivity;
import group01.itproject.MenuUICreator;
import group01.itproject.ResourcesManager;
import group01.gameObjects.Weapon;
import group01.itproject.SceneManager.SceneType;
import group01.itproject.Utils;


import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;

import org.andengine.entity.scene.menu.MenuScene;
import org.andengine.entity.scene.menu.MenuScene.IOnMenuItemClickListener;
import org.andengine.entity.scene.menu.item.IMenuItem;
import org.andengine.entity.scene.menu.item.SpriteMenuItem;
import org.andengine.entity.scene.menu.item.decorator.ScaleMenuItemDecorator;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.opengl.texture.region.ITiledTextureRegion;




public class GameScene extends BaseScene implements IOnMenuItemClickListener{
	
	//Constants
	//Integer constant for button events
	public static final String WIND= "wind";
	public static final String HIT= "hit";
	private final int PAUSE = 0;
	private final int GAME = 1;
	private final int MENU = 2;
	//Integer constant for weapon switch events
	
    private final float FRAME_DURATION = 3;
	
	private ArrayList<List<Float>> POSITION;
	//Profile
	
	//HUD
	private Text curScore;
	private Text Score;
	private int score = 0;
	private int otherScore=0;
	
	
	//Sub screen for game screen
	private MenuScene mainScene;
	private MenuScene pauseScene;
	
	//Background sprites of scene
	private SceneBG mainSceneBg;
	private SceneBG pauseSceneBg;
	private Sprite target;
    //Activity
	private GameActivity myActivity;
	
	//UICreator
	private MenuUICreator pauseMenuUI;
	
	//Weapon
	public ArrayList<Weapon> weapon;	
	public ITiledTextureRegion weaponTexture;
	private float weaponX;
	private float weaponY;
	
    //Wind
	private Sprite wind;
    private final float WIND_STRENGHT= 40;
    private boolean blowing=false;
    private Text MyMsg;
    

    //Offset values used when generating Menu backgrounds and buttons
	private float midMenuX;
	private float midMenuY;

	final private float MENU_BORDER_X = 25;
	final private float MENU_BORDER_Y = 25;
	
	
	public GameScene(){

		
		
	}
	public void initActivity(GameActivity act){
		myActivity=act;
	}
	
	@Override
	/**
	 * createScene, initialise game scene
	 */
	
	public void createScene() {
		// TODO Auto-generated method stub
		initialiseVariable();
	    createBackground();
	    createSubScenesBg();
	    createmainScene();
	    createPauseScene();
	    //createWSelectionScene();
	    createTarget();
	    //createWeapon(weaponTexture,Utils.userIndex);
	    //createSpriteSpawnTimeHandler();
	    createHUD();
	}
	/**
	 * iniitaliseVariable, set variables used in game
	 */
	private void initialiseVariable(){
		weapon=new ArrayList<Weapon>();
		POSITION=new ArrayList<List<Float>>();
		POSITION.add(Arrays.asList(camera.getCenterX()-(camera.getCenterX()/2)
			,camera.getYMax() - resourcesManager.basic.getHeight()));
		POSITION.add(Arrays.asList(camera.getCenterX()+(camera.getCenterX()/2)
			,camera.getYMax() - resourcesManager.basic.getHeight()));
		midMenuX = camera.getCenterX()-(resourcesManager.subbg.getWidth()/2);
		midMenuY = camera.getCenterY()-(resourcesManager.subbg.getHeight()/2);
		otherScore =0;// Integer.parseInt(profile.getScore());
		weaponTexture = resourcesManager.basic;
	}
			
	
	/**
	 * createBackground, create and load main background of the game
	 */
	private void createBackground()
	{   
		mainSceneBg = new SceneBG(0,0, resourcesManager.gamebg, vbom);
		attachChild(mainSceneBg);
	}
	
	/**
	 * createSubScenesBg, create menu backgrounds of the game
	 */
	private void createSubScenesBg(){
		pauseSceneBg = new SceneBG(0,0, resourcesManager.subbg, vbom);
	}
	
	/**
	 * createWeapon
	 * @param texture - Texture for the weapon sprite
	 * creates weapon using given texture
	 */
	public void createWeapon(ITiledTextureRegion texture, final int i){
		weaponX = POSITION.get(i).get(0);
		weaponY = POSITION.get(i).get(1);
		
		Weapon weaponI = new Weapon(weaponX,weaponY,texture,vbom,FRAME_DURATION){
			@Override
			//Action to do after weapon was thrown
			protected void onManagedUpdate(float pSecondsElapsed) 
		    {
		        //Out of bound, delete weapon and create new one
                if (this.mY>camera.getYMax()+20||this.mY<camera.getYMin()-20||
                		this.mX>camera.getXMax()+20||this.mX<camera.getXMin()-20){
                	this.detachSelf();
		            this.dispose();
		            createWeapon(weaponTexture,i);
                }
                //Out of bound, delete weapon and create new one
			    else if (this.mX<(this.initX-this.stopX)||this.mX>(this.initX+this.stopX)
			    		&&this.stopX!=0)
		        {
		            this.detachSelf();
		            this.dispose();
		            createWeapon(weaponTexture,i);
		        }
                //Target was hit
		        else if(this.collidesWith(target)&&this.getScaleX()<=0.5f){
		        	if(i==Utils.userIndex){
		             updateScore();
		        	}
		        	this.detachSelf();
			        this.dispose();
			        createWeapon(weaponTexture, i);
		        }
		        super.onManagedUpdate(pSecondsElapsed);
		    }
		};
	    
		registerTouchArea(weaponI);
		setTouchAreaBindingOnActionDownEnabled(true);
		attachChild(weaponI);
		if ((weapon.size()>i&& weapon.size()!=0)){
			weapon.set(i, weaponI);
		}
		else{
	        weapon.add(weaponI);
		}
	
		
	}

	/**
	 * setWeaponAvailable 
	 * @param avail - Whether weapon is available or not
	 * Able/Disable weapon object to be thrown
	 */
	private void setWeaponAvailable(Boolean avail){
		for(int i=0; i <weapon.size(); i++){
		weapon.get(i).setAvaiable(avail);
		}
	}
    /**
     * Update Score
     */
	private void updateScore()
	{
		myActivity.getHit(HIT);
	}
	/**
	 * createTarget, create and load target sprite
	 */
	private void createTarget(){
    	float targetX=camera.getCenterX()+100;
    	float targetY=camera.getCenterY()-70;
    	target= new Sprite (targetX, targetY, resourcesManager.target, vbom);
    	attachChild(target);
    }
	
	
	/**
	 * createmainScene, create mainScene of the game
	 */
	private void createmainScene()
	{
		//Create MenuScene which will hold buttons for title screen
		mainScene = new MenuScene(camera);
		mainScene.setPosition(0, 0);
	    
		//Create buttons for the title screen
	    final IMenuItem pausebtn = new ScaleMenuItemDecorator(
	    		new SpriteMenuItem(PAUSE, resourcesManager.pausebtn, vbom), 1.2f, 1);

	    
	    //Add buttons to its scene
	    mainScene.addMenuItem(pausebtn);
	    //Activate buttons
	    mainScene.buildAnimations();
	    mainScene.setBackgroundEnabled(false);
	    
	    //Position buttons appropriately
	    pausebtn.setPosition(camera.getWidth() - pausebtn.getWidth() , 
	    		0);
	    
	    //Create listeners for each button
	    //The listener uses constant value defined in this class
	    //To distinguish which button has been clicked.
	    mainScene.setOnMenuItemClickListener(this);
	    //Set scene.
	    setChildScene(mainScene);
	}
	
	
	
	/**
	 * createPauseScene, create pause scene of the game
	 */
	private void createPauseScene()
	{
		
		//Create MenuScene which will hold buttons for title screen
		pauseScene = new MenuScene(camera);
		pauseScene.setPosition(midMenuX, midMenuY);
		
	    pauseScene.buildAnimations();
	    pauseScene.setBackgroundEnabled(false);
		pauseScene.attachChild(pauseSceneBg);
		
		//Create buttons
		pauseMenuUI = new MenuUICreator(pauseScene, pauseSceneBg, midMenuX+MENU_BORDER_X, midMenuY-MENU_BORDER_Y);
	    pauseMenuUI.addButtons(GAME, resourcesManager.gamebtn, vbom);
	    pauseMenuUI.addButtons(MENU, resourcesManager.menubtn, vbom);
	    

	    pauseScene.setOnMenuItemClickListener(this);
	    //Set scene.
	}
	
	/**
	 * onMenuItemClicked, listner for UI interaction
	 */
	public boolean onMenuItemClicked(MenuScene pMenuScene, IMenuItem pMenuItem, float pMenuItemLocalX, float pMenuItemLocalY)
	{
		//Actions to perform whenever buttons clicked
	        switch(pMenuItem.getID())
	        {
	        case PAUSE:
	        	myActivity.theClient.stopGame();
	        	setWeaponAvailable(false);
	        	setChildScene(pauseScene);
	            return true;
	        case GAME:
	        	setWeaponAvailable(true);
	        	setChildScene(mainScene);
	        	myActivity.theClient.startGame();
	        	return true;
	        case MENU:
	        	endGame();
	        	return true;
	        default:
	            return false;
	    }
	}
	
	
	/**
	 * endGame terminate game scene and move to menu scene
	 */
	public void endGame(){
    	camera.setHUD(null);
    	//resourcesManager.pHandler.updateProfile(profile);
    	myActivity.stopGame();
	}
	
	/**
	 * createHUD, create and load HUD for the game scene
	 */
	private void createHUD(){
		//otherScore of the player
		Score = new Text(20,0, resourcesManager.font, "Player 2's score: 0123456789",vbom);
		Score.setSkewCenter(0,0);
		Score.setText("Player 2's score: " + otherScore);
		
		//Current score of the player
		curScore = new Text(20, 420, resourcesManager.font, "My Score: 0123456789", vbom);
		curScore.setSkewCenter(0, 0);    
		curScore.setText("My Score " + score);
		this.attachChild(Score);
		this.attachChild(curScore);
		//hud.attachChild(Score);
		//hud.attachChild(curScore);
	    //camera.setHUD(hud);
		
	}
	
	
	/**
	 * Inform players about their turns
	 * @param msg
	 */
	public void anounceText(final String msg){
		//othre of the player
		MyMsg = new Text(camera.getCenterX(),camera.getCenterY(), resourcesManager.font, msg,vbom);
		MyMsg.setSkewCenter(0,0);
		MyMsg.setPosition(camera.getCenterX() - MyMsg.getWidth()/2f, camera.getCenterY() - MyMsg.getHeight()/2f);
		MyMsg.setText(msg);
		this.attachChild(MyMsg);
		this.registerUpdateHandler( new TimerHandler(ResourcesManager.FLASH, new ITimerCallback()
		{
		
		@Override
		public void onTimePassed(final TimerHandler pTimerHandler)
		{         
				//after 4 seconds, stop showing
			if(msg.equals(ResourcesManager.ULOSE)||msg.equals(ResourcesManager.UWIN)){
				MyMsg.detachSelf();
				MyMsg.dispose();
				if(Utils.userIndex==0){
					endGame();
				}
			}
			else{
			MyMsg.detachSelf();
			MyMsg.dispose();
			
			}
		}
				
				
		}
		
		)
		
				);
		
	}

	
	/**
	 * addToScore
	 * @param i - add i to score
	 * add to score
	 */
	public void addToScore(int i)
	{
	    score =i;
	    curScore.setText("My Score: " + score);
	      
	    
	}
	
	/**
	 * addToScore
	 * @param i - add i to score
	 * add to score of the opponent
	 */
	public void addToOtherScore(int i)
	{
	    	//resourcesManager.player.setScore(String.valueOf(score));
	    	otherScore =i;
	    	Score.setText("Player 2's score: " + otherScore);
	    	
	    
	}
	
	

	@Override
	public void onBackKeyPressed() {
		// TODO Auto-generated method stub
		
	}        


	@Override
	public SceneType getSceneType() {
		// TODO Auto-generated method stub
		return SceneType.SCENE_GAME;
	}

	@Override
	public void disposeScene() {
		// TODO Auto-generated method stub
		
	}
	// ______________WIND METHODS_____________________________________
	public void createWind() {
		float pX=camera.getXMin()+20;
		float pY=camera.getCenterY()-100;
		blowing=true;
		wind = new Sprite(pX, pY, resourcesManager.wind, vbom);
		attachChild(wind);
		this.registerUpdateHandler( new TimerHandler(ResourcesManager.DURATION, new ITimerCallback()
			{                      
			@Override
			public void onTimePassed(final TimerHandler pTimerHandler)
				{          
					//after 10 seconds, stop blowing
					wind.detachSelf();
					wind.dispose();
					for(int i =0; i<weapon.size(); i++){
					weapon.get(i).getPhysics().setAccelerationX(0);
					}
					blowing=false;               
				}
			}
			){
			@Override
			public void onUpdate(final float pSecondsElapsed){
				super.onUpdate(pSecondsElapsed);
				//affect object's path
				for(int i =0; i<weapon.size(); i++){
				if(weapon.get(i).getThrown()&& weapon.get(i).getY() >=camera.getCenterY()&&blowing){
					weapon.get(i).getPhysics().setAccelerationX(WIND_STRENGHT);
					}
				}
				}
			}
		);
	}
				
	/**
	 * createSpriteSpawnTimeHandler
	 * handler which creates wind given WIND INTERVAL
	 * generates wind on every seconds (based on WIND_INTERVAL constant)
	 */
	public void createSpriteSpawnTimeHandler(){
		this.engine.registerUpdateHandler( new TimerHandler(ResourcesManager.WIND_INTERVAL,true, new ITimerCallback()
			{                      
				@Override
				public void onTimePassed(final TimerHandler pTimerHandler){          
					//wind blow every 30 seconds 
					myActivity.sendChange(WIND);
				}
			}));
	}
	
			      		 


}
