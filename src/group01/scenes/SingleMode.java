/**
 * Author: Ha Jin Song
 * Date: 30/9/2014
 * Last Modified: 28/10/2014
 * Game Scene for single player
 */

package group01.scenes;

import group01.gameObjects.Player;
import group01.gameObjects.WeaponSingle;
import group01.itproject.MenuUICreator;
import group01.itproject.ResourcesManager;
import group01.itproject.SelectionUICreator;
import group01.itproject.SceneManager.SceneType;
import group01.itproject.SingleModeActivity;


import org.andengine.engine.camera.hud.HUD;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;

import org.andengine.entity.scene.menu.MenuScene;
import org.andengine.entity.scene.menu.MenuScene.IOnMenuItemClickListener;
import org.andengine.entity.scene.menu.item.IMenuItem;
import org.andengine.entity.scene.menu.item.SpriteMenuItem;
import org.andengine.entity.scene.menu.item.decorator.ScaleMenuItemDecorator;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.opengl.texture.region.ITiledTextureRegion;



public class SingleMode extends BaseScene implements IOnMenuItemClickListener{
	
	//Constants
	//Integer constant for button events
	private final int PAUSE = 0;
	private final int GAME = 1;
	private final int MENU = 2;
	private final int WEAPON = 3;
	//Integer constant for weapon switch events
	private final int BASIC = 100;
	private final int SPECIAL = 101;
	
    private final float FRAME_DURATION = 3;
	// activity
    SingleModeActivity myActivity;
	
	//Profile
    private Player profile;
	
	
	//HUD
	private HUD hud;
	private Text curScore;
	private Text Score;
	private int score = 0;
	private int highscore;
	
	
	//Sub screen for game screen
	private MenuScene mainScene;
	private MenuScene pauseScene;
	private MenuScene weaponScene;
	private MenuScene endScene;
	//Background sprites of scene
	private SceneBG mainSceneBg;
	private SceneBG weaponSceneBg;
	private SceneBG pauseSceneBg;
	private SceneBG endSceneBg;
	private Sprite target;

	private MenuUICreator pauseMenuUI;
	private SelectionUICreator SelectWUI;
	private MenuUICreator endMenuUI;
	
	//Weapon
	private WeaponSingle weapon;	
	private ITiledTextureRegion weaponTexture;
	private float weaponX;
	private float weaponY;
	
    //Wind
	private Sprite wind;
    private final float WIND_STRENGHT= 40;
    private boolean blowing=false;
    public static final float WIND_DURATION=10;
    public static float WIND_INTERVAL=30;

    //Teacher
	private Sprite teacher;
    private boolean watching=false;
    public static final float TEACHER_DURATION=10;
    public static float TEACHER_INTERVAL=40;
    
    //Offset values used when generating Menu backgrounds and buttons
	private float midMenuX;
	private float midMenuY;
	final private float MENU_BORDER_X = 25;
	final private float MENU_BORDER_Y = 25;
	@Override
	
	/**
	 * createScene, initialise game scene
	 */
	public void createScene() {
		// TODO Auto-generated method stub
		initialiseVariable();
		
		
	    createBackground();
	    createSubScenesBg();
	    
	    createmainScene();
	    createPauseScene();
	    createWSelectionScene();
	    
	    createTarget();
	    createWeapon(weaponTexture);
	    createWindSpawnHandler();
	    createTeacherHandler();

	    createHUD();
	}
	
	/**
	 * set activity
	 */
	public void setActivity(SingleModeActivity a){
		myActivity=a;
	}
	/**
	 * iniitaliseVariable, set variables used in game
	 */
	private void initialiseVariable(){
		profile = ResourcesManager.player;
		midMenuX = camera.getCenterX()-(resourcesManager.subbg.getWidth()/2);
		midMenuY = camera.getCenterY()-(resourcesManager.subbg.getHeight()/2);
		highscore = Integer.parseInt(profile.getScore());
		weaponTexture = resourcesManager.special;
	}
			
	
	/**
	 * createBackground, create and load main background of the game
	 */
	private void createBackground()
	{
		mainSceneBg = new SceneBG(0,0, resourcesManager.gamebg, vbom);
		attachChild(mainSceneBg);
	}
	
	/**
	 * createSubScenesBg, create menu backgrounds of the game
	 */
	private void createSubScenesBg(){
		pauseSceneBg = new SceneBG(0,0, resourcesManager.subbg, vbom);
		weaponSceneBg = new SceneBG(0,0, resourcesManager.subbg, vbom);
		endSceneBg = new SceneBG(0,0, resourcesManager.subbg,vbom);
	}
	
	/**
	 * createWeapon
	 * @param texture - Texture for the weapon sprite
	 * creates weapon using given texture
	 */
	private void createWeapon(ITiledTextureRegion texture){
		weaponX = camera.getCenterX();
		weaponY = camera.getYMax() - resourcesManager.basic.getHeight();
		
		weapon = new WeaponSingle(weaponX,weaponY,texture,vbom,FRAME_DURATION){
			@Override
			//Action to do after weapon was thrown
			protected void onManagedUpdate(float pSecondsElapsed) 
		    {
		        //Out of bound, delete weapon and create new one
                if (this.mY>camera.getYMax()+20||this.mY<camera.getYMin()-20||
                		this.mX>camera.getXMax()+20||this.mX<camera.getXMin()-20){
                	this.detachSelf();
		            this.dispose();
		            createWeapon(weaponTexture);
                }
                //Out of bound, delete weapon and create new one
			    else if (this.mX<(this.initX-this.stopX)||this.mX>(this.initX+this.stopX)
			    		&&this.stopX!=0)
		        {
		            this.detachSelf();
		            this.dispose();
		            createWeapon(weaponTexture);
		        }
                //Target was hit
		        else if(this.collidesWith(target)&&this.getScaleX()<=0.5f){
		        	 addToScore(1);
		        	 this.detachSelf();
			         this.dispose();
			         createWeapon(weaponTexture);
		        }
		        super.onManagedUpdate(pSecondsElapsed);
		    }
		};
	
		registerTouchArea(weapon);
		setOnSceneTouchListener(weapon);
		setTouchAreaBindingOnActionDownEnabled(true);
		attachChild(weapon);
		
	}

	/**
	 * setWeaponAvailable 
	 * @param avail - Whether weapon is available or not
	 * Able/Disable weapon object to be thrown
	 */
	private void setWeaponAvailable(Boolean avail){
		weapon.setAvaiable(avail);
	}
    
	
	/**
	 * createTarget, create and load target sprite
	 */
	private void createTarget(){
    	float targetX=camera.getCenterX()+100;
    	float targetY=camera.getCenterY()-70;
    	target= new Sprite (targetX, targetY, resourcesManager.target, vbom);
    	attachChild(target);
    }
	
	
	/**
	 * createmainScene, create mainScene of the game
	 */
	private void createmainScene()
	{
		//Create MenuScene which will hold buttons for title screen
		mainScene = new MenuScene(camera);
		mainScene.setPosition(0, 0);
	    
		//Create buttons for the title screen
	    final IMenuItem pausebtn = new ScaleMenuItemDecorator(
	    		new SpriteMenuItem(PAUSE, resourcesManager.pausebtn, vbom), 1.2f, 1);
	    final IMenuItem selectWeaponBtn = new ScaleMenuItemDecorator(
	    		new SpriteMenuItem(WEAPON,resourcesManager.selectbtn,vbom),1.2f,1);
	    		
	    
	    //Add buttons to its scene
	    mainScene.addMenuItem(pausebtn);
	    mainScene.addMenuItem(selectWeaponBtn);
	    //Activate buttons
	    mainScene.buildAnimations();
	    mainScene.setBackgroundEnabled(false);
	    
	    //Position buttons appropriately
	    pausebtn.setPosition(camera.getWidth() - pausebtn.getWidth() , 
	    		0);
	    selectWeaponBtn.setPosition(0,
	    		camera.getHeight()/2);
	    
	    //Create listeners for each button
	    //The listener uses constant value defined in this class
	    //To distinguish which button has been clicked.
	    mainScene.setOnMenuItemClickListener(this);
	    //Set scene.
	    setChildScene(mainScene);
	}
	
	/**
	 * createWSelectionScene, create weapon selection scene of the game
	 */
	private void createWSelectionScene(){
		
		weaponScene = new MenuScene(camera);
		weaponScene.setPosition(midMenuX,midMenuY);
		weaponScene.attachChild(weaponSceneBg);
		weaponScene.buildAnimations();
		weaponScene.setBackgroundEnabled(false);
		//Create selection of weapons for the user
		SelectWUI = new SelectionUICreator(weaponScene,weaponSceneBg,MENU_BORDER_X,MENU_BORDER_Y);
		for(String s : profile.getWeapons()){
	        switch (s)
	        {
	        	
	            case "Special":
	            	SelectWUI.addButtons(SPECIAL, resourcesManager.special,vbom);
	                break;
	            default:
	            	SelectWUI.addButtons(BASIC, resourcesManager.basic,vbom);
	                break;
	        }
		}


	



		weaponScene.setOnMenuItemClickListener(this);
		
		
		
	}

	/**
	 * createPauseScene, create pause scene of the game
	 */
	private void createEndScene()
	{
		
		//Create MenuScene which will hold buttons for title screen
		endScene = new MenuScene(camera);
		endScene.setPosition(midMenuX, midMenuY);
		endScene.attachChild(endSceneBg);
		endScene.buildAnimations();
		endScene.setBackgroundEnabled(false);
		
		Text gameOver = new Text(midMenuX-MENU_BORDER_X, midMenuY/2, resourcesManager.font, "Sensei noticed you", vbom);
		gameOver.setSkewCenter(0, 0);    
		gameOver.setText("Sensei noticed you");
		
		Text finalScore = new Text(midMenuX-MENU_BORDER_X, midMenuY, resourcesManager.font, "Final Score: 0123456789", vbom);
		finalScore.setSkewCenter(0, 0);    
		finalScore.setText("Final Score: " + score);
		
		endScene.attachChild(gameOver);
		endScene.attachChild(finalScore);
		//Create buttons
		endMenuUI = new MenuUICreator(endScene, endSceneBg, midMenuX, (float) (midMenuY*1.5));
		endMenuUI.addButtons(MENU, resourcesManager.menubtn, vbom);
	    

		endScene.setOnMenuItemClickListener(this);
	    //Set scene.
	}
	
	/**
	 * createPauseScene, create pause scene of the game
	 */
	private void createPauseScene()
	{
		
		//Create MenuScene which will hold buttons for title screen
		pauseScene = new MenuScene(camera);
		pauseScene.setPosition(midMenuX, midMenuY);
		
	    pauseScene.buildAnimations();
	    pauseScene.setBackgroundEnabled(false);
		pauseScene.attachChild(pauseSceneBg);
		
		//Create buttons
		pauseMenuUI = new MenuUICreator(pauseScene, pauseSceneBg, midMenuX+MENU_BORDER_X, midMenuY-MENU_BORDER_Y);
	    pauseMenuUI.addButtons(GAME, resourcesManager.gamebtn, vbom);
	    pauseMenuUI.addButtons(MENU, resourcesManager.menubtn, vbom);
	    

	    pauseScene.setOnMenuItemClickListener(this);
	    //Set scene.
	}
	
	/**
	 * onMenuItemClicked, listner for UI interaction
	 */
	public boolean onMenuItemClicked(MenuScene pMenuScene, IMenuItem pMenuItem, float pMenuItemLocalX, float pMenuItemLocalY)
	{
		//Actions to perform whenever buttons clicked
	        switch(pMenuItem.getID())
	        {
	        case PAUSE:
	        	setWeaponAvailable(false);
	        	setChildScene(pauseScene);
	            return true;
	        case GAME:
	        	setWeaponAvailable(true);
	        	setChildScene(mainScene);
	        	return true;
	        case MENU:
	        	endGame();
	        	return true;
	        case WEAPON:
	        	setWeaponAvailable(false);
	        	setChildScene(weaponScene);
	        	return true;
	        case BASIC:
	        	changeWeapon(resourcesManager.basic);
	        	return true;
	        case SPECIAL:
	        	changeWeapon(resourcesManager.special);
	        	return true;
	        default:
	            return false;
	    }
	}
	
	/**
	 * changeWeapon
	 * @param texture - Texture for new weapon
	 * Change weapon using given texture
	 */
	private void changeWeapon(ITiledTextureRegion texture){
    	weaponTexture = texture;
    	setWeaponAvailable(true);
    	weapon.detachSelf();
    	weapon.dispose();
        createWeapon(weaponTexture);
    	setChildScene(mainScene);
	}
	

	/**
	 * endGame terminate game scene and move to menu scene
	 */
	private void endGame(){
    	camera.setHUD(null);
    	ResourcesManager.pHandler.updateProfile(profile);
    	myActivity.stopGame();
	}
	
	/**
	 * createHUD, create and load HUD for the game scene
	 */
	private void createHUD(){
		hud = new HUD();

		//highscore of the player
		Score = new Text(20,0, resourcesManager.font, "Highscore: 0123456789",vbom);
		Score.setSkewCenter(0,0);
		Score.setText("Highscore: " + highscore);
		
		//Current score of the player
		curScore = new Text(20, 420, resourcesManager.font, "Score: 0123456789", vbom);
		curScore.setSkewCenter(0, 0);    
		curScore.setText("Score: " + score);
		
		hud.attachChild(Score);
		hud.attachChild(curScore);
	    camera.setHUD(hud);
		
	}

	
	/**
	 * addToScore
	 * @param i - add i to score
	 * add to score
	 */
	private void addToScore(int i)
	{
	    score += i;
	    curScore.setText("Score: " + score);
	    //If user makes new highscore, update their profile and textview
	    if(score > highscore){
	    	profile.setScore(String.valueOf(score));
	    	highscore = score;
	    	Score.setText("Highscore: " + highscore);
	    }
	    
	}
	
	

	@Override
	public void onBackKeyPressed() {
		// TODO Auto-generated method stub
		
	}        


	@Override
	public SceneType getSceneType() {
		// TODO Auto-generated method stub
		return SceneType.SCENE_GAME;
	}

	@Override
	public void disposeScene() {
		// TODO Auto-generated method stub
		
	}
	// ______________WIND METHODS_____________________________________
	/**
	 * createWind
	 * Draw wind on the game scene and set listener for it
	 */
	private void createWind() {
		float pX=camera.getXMin()+20;
		float pY=camera.getCenterY()-100;
		blowing=true;
		wind = new Sprite(pX, pY, resourcesManager.wind, vbom);
		attachChild(wind);
		this.registerUpdateHandler( new TimerHandler(WIND_DURATION, new ITimerCallback()
			{                      
			@Override
			public void onTimePassed(final TimerHandler pTimerHandler)
				{          
					//after duration of the wind, stop blowing
					wind.detachSelf();
					wind.dispose();
					weapon.getPhysics().setAccelerationX(0);
					blowing=false;               
				}
			}
			){
			@Override
			public void onUpdate(final float pSecondsElapsed){
				super.onUpdate(pSecondsElapsed);
				//affect object's path
				if(weapon.getThrown()&& weapon.getY() >=camera.getCenterY()&&blowing){
					weapon.getPhysics().setAccelerationX(WIND_STRENGHT);
					}
				}
			}
		);
	}
				
	private void createWindSpawnHandler(){
		this.engine.registerUpdateHandler( new TimerHandler(WIND_INTERVAL,true, new ITimerCallback()
			{                      
				@Override
				public void onTimePassed(final TimerHandler pTimerHandler){          
					//wind blow every 30 seconds 
					createWind();
				}
			}));
	}
	
	/**
	 * createTeacher
	 * Draw teacher on the game scene and set listener for it
	 */
	private void createTeacher() {
		//Draw teacher on location and set values
		float pX=camera.getXMin()+250;
		float pY=camera.getCenterY()-100;
		watching=true;
		teacher = new Sprite(pX, pY, resourcesManager.teacher, vbom);
		attachChild(teacher);
		this.registerUpdateHandler( new TimerHandler(TEACHER_DURATION, new ITimerCallback()
			{                      
			@Override
			public void onTimePassed(final TimerHandler pTimerHandler)
				{          
					//after duration passes, stop watching
					teacher.detachSelf();
					teacher.dispose();
					watching=false;               
				}
			}
			){
			@Override
			public void onUpdate(final float pSecondsElapsed){
				super.onUpdate(pSecondsElapsed);
				//Detected by teacher, end game
				if(weapon.getThrown()&&watching){
						setWeaponAvailable(false);
						if(endScene==null){
							createEndScene();
						}
						setChildScene(endScene);
					}
				}
			}
		);
	}
	
	/**
	 * createTeacherHandler, turn teacher on and off
	 */
	private void createTeacherHandler(){
		this.engine.registerUpdateHandler( new TimerHandler(TEACHER_INTERVAL,true, new ITimerCallback()
		{                      
			@Override
			public void onTimePassed(final TimerHandler pTimerHandler){           
				createTeacher();
			}
		}));
		
		
	}
			      		 


}
