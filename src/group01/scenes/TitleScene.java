package group01.scenes;

import group01.itproject.MenuUICreator;
import group01.itproject.SceneManager;
import group01.itproject.SceneManager.SceneType;


import org.andengine.entity.scene.menu.MenuScene;
import org.andengine.entity.scene.menu.MenuScene.IOnMenuItemClickListener;
import org.andengine.entity.scene.menu.item.IMenuItem;


/**
 * Author: Ha Jin Song
 * Date: 30/9/2014
 * Last Modified: 30/9/2014
 * Scene for TitleScene
 * Based on tutorial for AndEngine
 * http://www.matim-dev.com/tutorials.html
 */

public class TitleScene extends BaseScene implements IOnMenuItemClickListener{
	
	private MenuScene buttonScene;
	//Integer value for Actions
	private final int START = 0;
	private final int OPTION = 1;
	private final int BLUETOOTH = 2;

	private SceneBG mainSceneBg;
	private MenuUICreator titleMenuUI;
	@Override
	/**
	 * createScene constructor
	 */
	public void createScene() {
		createBackground();
		createbtnScene();
	}
	
	/**
	 * createBackground creates background for titleScene
	 */
	private void createBackground()
	{
		mainSceneBg = new SceneBG(0,0, resourcesManager.title_bg_reg, vbom);
	}
	
	/**
	 * createbtnScene create buttons
	 */
	private void createbtnScene()
	{
	    
		//Create MenuScene which will hold buttons for title screen
		buttonScene = new MenuScene(camera);
		buttonScene.setPosition(0, 0);
	    buttonScene.buildAnimations();
	    buttonScene.setBackgroundEnabled(false);
	    buttonScene.attachChild(mainSceneBg);
		titleMenuUI = new MenuUICreator(buttonScene, mainSceneBg,
				(camera.getWidth()/2) - resourcesManager.strt_region.getWidth()/2, 
				(camera.getHeight()/2));
		//Create buttons for the title screen

		titleMenuUI.addButtons(START, resourcesManager.strt_region, vbom);
		titleMenuUI.addButtons(OPTION, resourcesManager.opt_region, vbom);
		titleMenuUI.addButtons(BLUETOOTH, resourcesManager.bluetooth_region, vbom);
		//Set Listner
	    buttonScene.setOnMenuItemClickListener(this);
	    //Set scene.
	    setChildScene(buttonScene);
	    
	}
	public boolean onMenuItemClicked(MenuScene pMenuScene, IMenuItem pMenuItem, float pMenuItemLocalX, float pMenuItemLocalY)
	{
		//Actions to perform whenever buttons clicked
	        switch(pMenuItem.getID())
	        {
	        case START:
	        	SceneManager.getInstance().loadGameScene(engine);
	            return true;
	        case OPTION:
	            return true;
	        case BLUETOOTH:
	        	
	            return true;
	        default:
	            return false;
	    }
	}

	@Override
	public void onBackKeyPressed() {
		// TODO Auto-generated method stub
		System.exit(0);
		
	}

	@Override
	public SceneType getSceneType() {
		// TODO Auto-generated method stub
		return SceneType.SCENE_TITLE;
	}

	@Override
	public void disposeScene() {
		// TODO Auto-generated method stub
		
	}

}
