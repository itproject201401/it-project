package group01.scenes;
/**
 * Author: Ha Jin Song
 * Date: 30/9/2014
 * Last Modified: 30/9/2014
 * Scene for SplashScene
 * Based on tutorial for AndEngine
 * http://www.matim-dev.com/tutorials.html
 */

import group01.itproject.SceneManager.SceneType;

import org.andengine.engine.camera.Camera;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.util.GLState;

public class SplashScene extends BaseScene
{

	
	private Sprite splash;
    @Override
    //Create scene using sprite
    public void createScene()
    {
    	splash = new Sprite(0, 0, resourcesManager.splash_region, vbom)
    	{
    	    @Override
    	    protected void preDraw(GLState pGLState, Camera pCamera) 
    	    {
    	       super.preDraw(pGLState, pCamera);
    	       
    	       pGLState.enableDither();
    	    }
    	};
    	splash.setScale(1.5f);
    	splash.setPosition((CAMERA_HEIGHT/2)-(splash.getHeight()/2), (CAMERA_WIDTH/2)-(splash.getWidth()/2));
    	attachChild(splash);
    }

    @Override
    public void onBackKeyPressed()
    {

    }

    @Override
    public SceneType getSceneType()
    {
    	return SceneType.SCENE_SPLASH;
    }

    @Override
    public void disposeScene()
    {
        splash.detachSelf();
        splash.dispose();
        this.detachSelf();
        this.dispose();
    }
}
