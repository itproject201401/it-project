package group01.scenes;
/**
 * Author: Ha Jin Song
 * Date: 5/10/2014
 * Last Modified: 6/10/2014
 * Draws Background for Scenes
 * Creates background of the scene using given x and y coordinate
 * And texture given
 */
import org.andengine.engine.camera.Camera;
import org.andengine.entity.sprite.Sprite;

import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.util.GLState;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

public class SceneBG extends Sprite{

	public SceneBG(float pX, float pY,
			ITextureRegion pTextureRegion,
			VertexBufferObjectManager vbom) {
		super(pX, pY, pTextureRegion, vbom);
		// TODO Auto-generated constructor stub
	}
    protected void preDraw(GLState pGLState, Camera pCamera) 
    {
        super.preDraw(pGLState, pCamera);
        pGLState.enableDither();
    }
}
